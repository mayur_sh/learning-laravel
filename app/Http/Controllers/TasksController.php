<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
    public function index(){

    	$tasks = Task::all();	//eloquent
	    return view('tasks.index', compact('tasks'));
    }

    public function show(Task $task){
    	//$task = Task::find($id);	//elqouent
    	//return $task;
		return view('tasks.show', compact('task'));
    }
}
